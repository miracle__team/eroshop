$(document).ready(function() {
  
  //Burger menu
  $('.burger-wrap a').click(function(){
    $('.mobile-menu').toggleClass('active');
    return false;
  });
  
  $('.overlay').click(function() {
    $('.mobile-menu').removeClass('active');
  });
  
  // Top banner close
  
  $('.top-banner__close').click(function() {
    $('.top-banner').hide();
  });
  
  // Carousels
  
  $('.showcase').slick({
    dots: true,
    speed: 200
  });
  
  $('.carousel').slick({
    slidesToScroll: 1,
    slidesToShow: 5,
    speed: 200,
    responsive: [
    {
      breakpoint: 1360,
      settings: {
        slidesToShow: 4
      }
    },
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 3
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 2
      }
    }
  ]
  });
  
  $('.brends').slick({
    slidesToScroll: 1,
    slidesToShow: 6,
    speed: 200,
    responsive: [
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 5
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 4
      }
    }
  ]
  });
  
  $('.product-gallery-slider').slick({
    slidesToScroll: 1,
    slidesToShow: 4,
    speed: 200,
    responsive: [
    {
      breakpoint: 1360,
      settings: {
        slidesToShow: 3
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 4
      }
    }
  ]
  })
  
  // Helper
  
  $('.helper-sex-item').click(function() {
    $('.helper-sex-item').removeClass('active');
    $(this).addClass('active');
  });
  
  // Slider
  $( ".helper-slider" ).slider({
      range: true,
      min: 1,
      max: 100000,
      values: [ 1, 66999 ],
      slide: function( event, ui ) {
        $( ".helper-slider-amount" ).html( ui.values[ 0 ] + " - " + ui.values[ 1 ]  +
      " ₽");
      }
    });
    $( ".helper-slider-amount" ).html(  $( ".helper-slider" ).slider( "values", 0 ) + ' - ' + $( ".helper-slider" ).slider( "values", 1 ) +
      " ₽" );
  
  // Mobile search
    
  $(".mobile-search").click(function() {
    $('.search').addClass('active');
    return false;
  });
  
  // Filter slider
  if( $( ".filters-slider" ).length ) {
    
    $( ".filters-slider" ).slider({
      range: true,
      min: 1,
      max: 300000,
      values: [ 2166, 245566 ],
      slide: function( event, ui ) {
        $( ".filters__input1" ).val( ui.values[ 0 ] );
        $( ".filters__input2" ).val( ui.values[ 1 ] );
      }
    });

    $( ".filters-inputs input" ).change(function() {
      $( ".filters-slider" ).slider( "option", "values", [
        $( ".filters__input1" ).val(),
        $( ".filters__input2" ).val()
      ] );
    });
    
  }
  
  // Filters
  
  $('.filter-button').click(function() {
    $('.catalog-sidebar').toggleClass('active');
  });
  
  // Cabinet tabs
  
  $('.cabinet-tabs-item').click(function() {
    $('.cabinet-tabs-item').removeClass('active');
    $(this).addClass('active');
    $('.cabinet-item').hide();
    $('.cabinet-item').eq($(this).index()).fadeIn();
  });
  
  //Fancybox
  
  if( $('.fancybox').length ) {
    $('.fancybox').fancybox({
      padding: 0
    });
  }
  
  // Cart float
  
  var cart_top,
      cart_left,
      cart = $('.cart-float');
  
  function get_cart_float() {
    cart_top = cart.offset().top;
    cart_left = cart.offset().left;
  }
  
  $(window).resize(function() {
    get_cart_float();
  });
  
  get_cart_float();
  
  $(window).scroll(function() {
    if( $(this).scrollTop() > cart_top - 20 ) {
      cart.addClass('cart-fixed');
      cart.css({"left" : cart_left })
    }
    else {
      cart.removeClass('cart-fixed');
      cart.css({"left" : 'auto' })
    }
    if( $(this).scrollTop() > $('.another').offset().top - cart.outerHeight() - 80  ) {
      cart.addClass('cart-pos');
    }
    else {
       cart.removeClass('cart-pos');
    }
  });
  
});





